package com.spring.employee.service;

import java.util.List;

import com.spring.employee.entity.Employee;

public interface EmployeeService {

	Employee saveEmployee(Employee department);

	List<Employee> fetchEmployeeList();

}